﻿<?php include("top.php") ?>

<h3>Разбор адреса</h3>
<form method="POST" action="">
	<p><textarea name="addr" cols="50" rows="10"><?=(isset($_POST['addr']) ? $_POST['addr'] : 'индекс 111111, РФ, Удмуртская республика, г.Ижевск, ул.Ленина, д.50а')?></textarea></p>
	<input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $addr = htmlspecialchars(strip_tags($_POST['addr']));
    $reg = '/индекс\s|,\sг\.|,\sул\.|,\sд\.|,\s/';
    $out = preg_split($reg, $addr);
    echo '<table border=1>';
    echo '<tr><th style="width:200">Поле</th><th style="width:200">Значение</th></tr>';
    echo '<tr><td>Индекс</td><td>'.$out[1].'</td></tr>';
    echo '<tr><td>Страна</td><td>'.$out[2].'</td></tr>';
    echo '<tr><td>Республика</td><td>'.$out[3].'</td></tr>';
    echo '<tr><td>Город</td><td>'.$out[4].'</td></tr>';
    echo '<tr><td>Улица</td><td>'.$out[5].'</td></tr>';
    echo '<tr><td>Дом</td><td>'.$out[6].'</td></tr>';
    echo '</table>';
}
?>

<?php include("bottom.php") ?>