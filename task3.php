﻿<?php include("top.php") ?>
	
<h3>Вставить строку между словами</h3>
<form method="POST", action="">
    <p>Строка: <input name="x" autocomplete="off" value="<?=(isset($_POST['x']) ? $_POST['x'] : 'я иду домой после работы')?>" /></p>
    <input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $x = trim(htmlspecialchars($_POST['x']));
    echo 'Ответ: ' . f($x);
}

function f($x)
{
	$d = '(я молодец)';
	$x = preg_replace("/  +/", " ", $x);
	$words = explode(' ', $x);
	if(count($words) > 0)
	{
		$retval = $words[0];
		for($i = 1; $i < count($words); $i++)
			$retval .= ' '.$d.' '.$words[$i];
		return $retval;
	}
	else return $d;
}
?>
		
<?php include("bottom.php") ?>