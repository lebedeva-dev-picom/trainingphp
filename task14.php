﻿<?php include("top.php") ?>

<h3>JS + регулярные выражения</h3>
<form>
	<p>Фамилия:</br><input name="p1" value="Иванов"></p>
	<p>Имя:</br><input name="p2" value="Иван"></p>
	<p>Отчество:</br><input name="p3" value="Иванович"></p>
	<p>E-mail:</br><input name="p5" value="qwerty@mail.mail.ru"></p>
	<p>Телефон:</br><input name="p4" value="8 (999) 123-45-67"></p>
	<p><input type="button" onclick="fd(p1.value, p2.value, p3.value, p4.value, p5.value)" value="Отправить" /></p>
</form>
<div id="msg"></div>

<?php include("bottom.php") ?>

<script>

function fd(p1, p2, p3, p4, p5)
{
	var str;
	if (p1 == '' || p2 == '' || p3 == '' || p4 == '')
		str = 'Поля не заполнены!';
		
	else if (!(chkRus(p1) && chkRus(p2) && chkRus(p3)))
		str = 'ФИО должны быть на русском языке!';
		
	else if (!chkTel(p4))
		str = 'Неверный номер телефона!';
		
	else if (!chkEml(p5))
		str = 'Неверный e-mail!';
		
	else
		str = '<p>Здравстуйте, ' + p1 + ' ' + p2 + ' ' + p3 + '!</p>' +
		'<p>Ваш E-mail: ' + p5 + '</p>' +
		'<p>Ваш телефон: ' + p4 + '</p>';
	
	document.getElementById("msg").innerHTML = str;
}

function chkRus(p)
{
	var reg = /[\u0400-\u04FF]+/;
	return p.match(reg) == p;
}

function chkTel(p)
{
	var reg = /^[0-9][0-9\s-\(\)]*[0-9]$/;
	return p.match(reg) == p;
}

function chkEml(p)
{
	var reg = /^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/;
	return p.match(reg) == p;
}

</script>