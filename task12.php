﻿<?php include("top.php") ?>

<h3>Проверка номера телефона</h3>
<form method="POST" action="">
	<p><input type="text" size="50" name="tel" value="<?=(isset($_POST['tel']) ? $_POST['tel'] : 'два-три-I 2-3-1')?>" /></p>
	<input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $tel = htmlspecialchars($_POST['tel']);

    $separator = '[ \-\(\)]*';
    $num_word = 'один|два|три|четыре|пять|шесть|семь|восемь|девять|ноль';
    $rome = '(?<![IXV])(?:I?[XV]|[XV]?I{1,3})(?![IXV])';
    $reg = '/(?:(?:\d|' . $num_word . '|' . $rome . ')' . $separator . ')+/';

    preg_match($reg, $tel, $arr);
    echo '<p>Результат: '.($arr[0] == $tel ? 'TRUE' : 'FALSE').'</p>';
}
?>

<?php include("bottom.php") ?>
