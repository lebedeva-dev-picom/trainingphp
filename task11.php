﻿<?php include("top.php") ?>

<h3>URL (2)</h3>
<form method="POST" action="">
	<p><input type="text" size="50" name="url" value="<?=(isset($_POST['url']) ?  $_POST['url'] : 'http://www.site.site2.ru/catalogs/n15/ara/ff/12/3_n14')?>" /></p>
	<input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $url = htmlspecialchars(strip_tags($_POST['url']));
    echo '<p>Адрес: <a href="'.$url.'">'.$url.'</a></p>';
    preg_match('/(?<=.)\d/', $url, $a);
    preg_match('/\d(?=\_)/', $url, $b);
    preg_match_all('/(?<=n)\d/', $url, $c);

    echo '<p>Первая цифра: '.( count($a) > 0 ? $a[0] : 'не найдено').'</p>';
    echo '<p>Цифра до подчеркивания: '.( count($b) > 0 ? $b[0] : 'не найдено').'</p>';
    echo '<p>Цифра после последнего "n": '.( count($c[0]) > 0 ? $c[0][count($c[0]) - 1] : 'не найдено').'</p>';
}
?>

<?php include("bottom.php") ?>