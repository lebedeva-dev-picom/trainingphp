﻿<?php include("top.php") ?>

<h3>Текстовый файл</h3>
<form method="POST" enctype="multipart/form-data" action="">
    <p>Файл для проверки: <input name="userfile" type="file" /></p>
    <p>Поиск: <input type="text" name="text" value="<?=(isset($_POST['text']) ? $_POST['text'] : 'я молодец')?>" /></p>
    <input type="submit" name="submit" value="Проверить" />
</form>
<div id="result"></div>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $uploads = 'TrainingPHP' . DIRECTORY_SEPARATOR . 'uploads';
    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $uploads . DIRECTORY_SEPARATOR;
    $full_path = $uploaddir . basename($_FILES['userfile']['name']);

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $full_path)) {
        echo '<p>Файл "' . $_FILES['userfile']['name'] . '"</p>';
        $search = $_POST['text'];
        $handle = fopen($full_path, 'r');
        $orig = fgets($handle);
        fclose($handle);
        $count = substr_count($orig, $search);
        $result = str_replace($search, '', $orig);
        $result = str_replace('  ', ' ', $result);
        file_put_contents($full_path, $result);
        $download = 'uploads' . DIRECTORY_SEPARATOR . basename($_FILES['userfile']['name']);
        echo '<p>' . $orig . '</p>' . 
            '<p>' . $result . '</p>' . 
            '<p>Строка "' . $search . '" удалена ' . $count . ' раз(а)</p>' . 
            '<p><a href="' . $download . '" download>Скачать</a></p>';
    } else {
        echo '<p>Не удалось загрузить файл</p>';
    }
}
?>

<?php include("bottom.php") ?>