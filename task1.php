﻿<?php include("top.php") ?>
    
<h3>Bычислить значение функций</h3>
<form method="POST", action="">
    <p>x = <input name="x" type="number" autocomplete="off" value="<?=(isset($_POST['x'])?$_POST['x']:'2')?>" /></p>
    <p>n = <input name="n" type="number" autocomplete="off" value="<?=(isset($_POST['n'])?$_POST['n']:'3')?>" /></p>
    <input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $x = $_POST['x'];
    $n = $_POST['n'];
    $a1 = y1($x, $n);
    $a2 = y2($x, $n);
    $a3 = f($x);
    echo '<p><img src="/TrainingPHP/img/formula-1.gif"></p><p>' . $a1 . '</p>';
    echo '<p><img src="/TrainingPHP/img/formula-2.gif"></p><p>' . $a2 . '</p>';
    echo '<p><img src="/TrainingPHP/img/formula-3.gif"></p><p>' . $a3 . '</p>';
}

function y1($x, $n)
{
    $y = 0;
    for ($i = 1; $i <= $n; $i++) {
        $y += (exp($i * $x) + tan($i * $x)) / (3 * $i * $x);
    }
    return $y;
}

function y2($x, $n)
{
    $y = 0;
    for ($i = 1; $i <= $n; $i++) {
        $y += ((pow($i + 2, 2)) / $i) * $x;
    }
    return $y;
}

function f($x)
{
    if ($x < 2) {
        return 5 * $x + 3;
    } elseif ($x > 5) {
        return sin((2 * $x) / 3);
    } else {
        $f = 0;
        for ($j = 1; $j < 10; $j++) {
            $f += $x / ($x + pow($j, 2));
        }
        return $f;
    }
}
?>
        
<?php include("bottom.php") ?>