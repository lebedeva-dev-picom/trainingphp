﻿<?php include("top.php") ?>
	
<h3>Факториал числа</h3>
<form method="POST", action="">
    <p>x = <input name="x" type="number" autocomplete="off" value="<?=(isset($_POST['x']) ? $_POST['x'] : '5')?>" /></p>
    <input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    echo 'x! = '.fct($_POST['x']);
}

function fct($x)
{
	$retval = 1;
	for($i = 1; $i <= $x; $i++) {
		$retval *= $i;
    }
	return $retval;
}
?>
		
<?php include("bottom.php") ?>
