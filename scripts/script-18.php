﻿<?php
$name = htmlspecialchars(strip_tags($_POST['name']));
$phone = htmlspecialchars(strip_tags($_POST['phone']));
$email = htmlspecialchars(strip_tags($_POST['email']));
$msg = htmlspecialchars(strip_tags($_POST['msg']));

$answ_msg = '';
$a1 = chkName($name);
$a2 = chkPhone($phone);
$a3 = chkEmail($email);

if ($a1 == 0 && $a2 == 0 && $a3 == 0) {
	$answ_msg = 'Спасибо. Администратор свяжется с Вами в ближайшее время';
}
echo '({"errf":{"name":"' . $a1 . '","phone":"' . $a2 . '","email":"' . $a3 . '"},"msg":"' . $answ_msg . '"})';

function chkName($name)
{
	global $answ_msg;
	if ($name == '') {
		$answ_msg .= 'Поле \"Имя\" должно быть заполнено</br>';
		return 1;
	}
	else return 0;
}

function chkPhone($phone)
{
	global $answ_msg;
	if ($phone == '') {
		$answ_msg .= 'Поле \"Телефон\" должно быть заполнено</br>';
		return 1;
	} else {
		$reg = '/^[0-9][0-9\s-\(\)]*[0-9]$/';
		preg_match($reg, trim($phone), $out);
		if ($out[0] != trim($phone)) {
			$answ_msg .= 'Неверный телефон</br>';
			return 1;
		} else return 0;
	}
}

function chkEmail($email)
{
	global $answ_msg;
	if ($email == '') {
		$answ_msg .= 'Поле \"E-mail\" должно быть заполнено</br>';
		return 1;
	} else {
		$reg = '/^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/';
		preg_match($reg, trim($email), $out);
		if ($out[0] != trim($email)) {
			$answ_msg .= 'Неверный E-mail</br>';
			return 1;
		} else return 0;
	}
}

?>