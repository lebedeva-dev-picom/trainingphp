﻿<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="../style.css">

<div id="popupform">
<form>
	<p><label>Имя</label>
	<input id="name" value="Вася Пупкин" class="input_text" /></p>
	<p><label>Телефон</label>
	<input id="phone" value="123456" class="input_text" /></p>
	<p><label>E-mail</label>
	<input id="email" value="v.pupkin@mail.ru" class="input_text" /></p>
	<p><label>Сообщение</label>
	<textarea id="message"  class="input_text"></textarea></p>
	<input id="sendButton" type="button" value="Отправить" />
</form>
<div id="answer" style="width: 360px"></div>
</div>
<script>

function removeErrorClass1()
{
	$('#name').removeClass();
	$('#name').addClass('input_text');
}

function removeErrorClass2()
{
	$('#email').removeClass();
	$('#email').addClass('input_text');
}

function removeErrorClass3()
{
	$('#phone').removeClass();
	$('#phone').addClass('input_text');
}

$('#name').focus(removeErrorClass1);
$('#email').focus(removeErrorClass2);
$('#phone').focus(removeErrorClass3);

function show_answer(data, textStatus)
{	
	var result = eval(data);
	$('#answer').html(result.msg);
	for(var a in result.errf)
	{
		if (result.errf[a] == '1')
		{
			$('#' + a).removeClass('input_text');
			$('#' + a).addClass('error_input_text');
		}
		else
		{
			$('#' + a).removeClass('error_input_text');
			$('#' + a).addClass('input_text');
		}
	}
}

function send()
{
	$.ajax({
		data: {
			'name': $('#name').val(),
			'phone': $('#phone').val(),
			'email': $('#email').val(),
			'message': $('#message').val()},
		url: 'script-18.php',
		type: 'POST',
		success: show_answer
	});
}

$('#sendButton').click(send);

</script>