﻿<?php include("top.php") ?>

<h3>PHP</h3>
<form action="" method="POST">
	<p>Фамилия:</br><input name="p1" value="<?=(isset($_POST['p1']) ? $_POST['p1'] : 'Иванов')?>"></p>
	<p>Имя:</br><input name="p2" value="<?=(isset($_POST['p2']) ? $_POST['p2'] : 'Иван')?>"></p>
	<p>Отчество:</br><input name="p3" value="<?=(isset($_POST['p3']) ? $_POST['p3'] : 'Иванович')?>"></p>
	<p>E-mail:</br><input name="p5" value="<?=(isset($_POST['p5']) ? $_POST['p5'] : 'qwerty@mail.mail.ru')?>"></p>
	<p>Телефон:</br><input name="p4" value="<?=(isset($_POST['p4']) ? $_POST['p4'] : '8 (999) 123-45-67')?>"></p>
	<p>Дата рождения:</br><input type="date" name="p6" value="<?=(isset($_POST['p6']) ? $_POST['p6'] : '1990-07-07')?>"></p>
	<p><input type="submit" name="submit" /></p>
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $Surname = htmlspecialchars(strip_tags($_POST['p1']));
    $Name = htmlspecialchars(strip_tags($_POST['p2']));
    $MidName = htmlspecialchars(strip_tags($_POST['p3']));
    $Phone = htmlspecialchars(strip_tags($_POST['p4']));
    $Email = htmlspecialchars(strip_tags($_POST['p5']));
    $BirthDate = htmlspecialchars(strip_tags($_POST['p6']));

    $err = '';

    if (
        $Surname == '' || 
        $Name == '' || 
        $MidName == '' || 
        $Phone == '' || 
        $Email == '' || 
        $BirthDate == ''
    ) {
        $err .= '<p>Не все поля заполнены!</p>';
    }
    if (fNoRus($Surname)) {
        $err .= '<p>Фамилия должна быть на русском языке!</p>';
    }
    if (fNoRus($Name)) {
        $err .= '<p>Имя должно быть на русском языке!</p>';
    }
    if (fNoRus($MidName)) {
        $err .= '<p>Отчество должно быть на русском языке!</p>';
    }
    if (noCorrectEml($Email)) {
        $err .= '<p>Неверный e-mail!</p>';
    }
    if (noCorrectPhone($Phone)) {
        $err .= '<p>Неверный номер телефона!</p>';
    }
    
    if ($err == '') {
        echo printWelcome($Surname, $Name, $MidName, $BirthDate, $Phone, $Email);
    } else {
        echo $err;
    }
}

function fNoRus($p)
{
	$reg = '/^[А-Яа-яЁё][А-Яа-яЁё-\s]*/u';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}

function noCorrectPhone($p)
{
	$reg = '/^[0-9][0-9\s-\(\)]*[0-9]$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}

function noCorrectEml($p)
{
	$reg = '/^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}
function getAge($p)
{
	$now_date = time();
	$brt_date = strtotime($p);
	$age = floor(($now_date - $brt_date) / (60 * 60 * 24 * 365.25));
	return $age;
}

function getUsername($p1, $p2, $p3, $p6)
{
	if (getAge($p6) > 18)
	return $p1.' '.$p2.' '.$p3;
	else return $p2;
}

function printForm($p1, $p2, $p3, $p4, $p5, $p6)
{
	$s = '<div>';
	$s .= '<h3>Приветствие</h3>';
	$s .= '<form action="" method="post">';
	$s .= '<p>Фамилия:</br><input name="p1" value="'.$p1.'"></p>';
	$s .= '<p>Имя:</br><input name="p2" value="'.$p2.'"></p>';
	$s .= '<p>Отчество:</br><input name="p3" value="'.$p3.'"></p>';
	$s .= '<p>E-mail:</br><input name="p5" value="'.$p5.'"></p>';
	$s .= '<p>Телефон:</br><input name="p4" value="'.$p4.'"></p>';
	$s .= '<p>Дата рождения:</br><input type="date" name="p6" value="'.$p6.'"></p>';
	$s .= '<p><input type="submit" /></p>';
	$s .= '</form>';
	$s .= '</div>';
	return $s;
}

function printWelcome($p1, $p2, $p3, $p6, $p4, $p5)
{
	$s = '<div id="msg"><p>Здравстуйте, '.getUsername($p1, $p2, $p3, $p6).'!</p></div>';
	$s .= '<p>Дата рождения: '.$p6.' ('.getAge($p6).')</p>';
	$s .= '<p>Телефон: '.$p4.'</p>';
	$s .= '<p>E-mail: '.$p5.'</p>';
	return $s;
}
?>

<?php include("bottom.php") ?>
