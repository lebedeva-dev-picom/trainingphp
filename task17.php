﻿<?php include("top.php") ?>

<h3>PHP + AJAX</h3>
<form>
    <p>Фамилия:</br><input name="p1" value="Иванов1"></p>
    <p>Имя:</br><input name="p2" value="Иван2"></p>
    <p>Отчество:</br><input name="p3" value="Иванович3"></p>
    <p>E-mail:</br><input name="p5" value="qwerty@mail.mail.ru4"></p>
    <p>Телефон:</br><input name="p4" value="8 (999) 123-45-67a"></p>
    <p>Дата рождения:</br><input type="date" name="p6" value="1990-07-07"></p>
    <p><input type="button" onclick="ff(p1.value, p2.value, p3.value, p4.value, p5.value, p6.value)" value="Отправить" /></p>
</form>
<div id="msg"></div>

<?php include("bottom.php") ?>

<script>
function ff(p1, p2, p3, p4, p5, p6)
{
    var params = 
        'p1=' + encodeURIComponent(p1) + 
        '&p2=' + encodeURIComponent(p2) + 
        '&p3=' + encodeURIComponent(p3) + 
        '&p4=' + encodeURIComponent(p4) + 
        '&p5=' + encodeURIComponent(p5) + 
        '&p6=' + encodeURIComponent(p6);
    var msg = document.getElementById('msg');
    var req = new XMLHttpRequest();
    
    req.onreadystatechange = function()
    {
        if (req.readyState == 4 && req.status == 200) {
            msg.innerHTML = req.responseText;
        }
    }
    req.open("POST", 'scripts/script-17.php', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(params);
}
</script>