﻿<?php include("top.php") ?>

<h3>Проверка текста</h3>
<form  method="POST" action="">
    <p><input type="text" name="p" value="<?=(isset($_POST['p']) ? $_POST['p'] : 'ab+c*')?>" /></p>
    <p><textarea name="t"><?=(isset($_POST['t']) ? $_POST['t'] : 'labcab chuiabbc uabc')?></textarea></p>
    <input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $text = htmlspecialchars($_POST['t']);
    $pattern = htmlspecialchars($_POST['p']);
    $count = preg_match_all('/'.$pattern.'/', $text, $arr);
    if ($count > 0) {
        echo '<p>Совпадений: '.$count.'</p>';
        foreach($arr[0] as $a) {
            echo '<li>'.$a.'</li>';
        }
    }
    else echo '<p>Совпадений не найдено</p>';
}
?>

<?php include("bottom.php") ?>