﻿<?php include("top.php") ?>
	
<h3>Текущая дата</h3>
<form method="POST", action="">
    <input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != ''):?>
    <table border="1">
        <tr><th style="width:120">Формат</th><th style="width:300">Дата</th></tr>
        <?php
        $dateFormats = array('Y-m-d', 'm.d.y', 'l dS of F Y', 'l \\t\h\e jS', 'r');
        foreach($dateFormats as $format):?>
            <tr><td><?=$format?></td><td><?=date($format)?></td></tr>
        <?php endforeach?>
    </table>
<?php endif;?>
		
<?php include("bottom.php") ?>