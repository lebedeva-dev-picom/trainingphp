﻿<?php include("top.php") ?>

<h3>Распознавание URL</h3>

<form  method="POST" action="">
	<p><input type="text" size="50" name="url" value="<?=(isset($_POST['url']) ? $_POST['url'] : 'https://yandex.ru/search/?lr=44&msid=1467786111.95713.22876.3334&text=')?>" /></p>
	<input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $t = htmlspecialchars($_POST['url']);
    $p = '~(?:(?:ftp|https?)?://|www\.)[a-z_.]+?[a-z_]{2,6}(:?/[a-z0-9\-?\[\]=&;#]+)?~i';
    echo '<p>URL: <a href="' . $t . '">' . $t . '</a></p><p>Результат распознавания: ';
    echo (preg_match_all($p, $t) ? 'TRUE' : 'FALSE');
}
?>

<?php include("bottom.php") ?>