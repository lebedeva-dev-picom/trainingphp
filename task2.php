﻿<?php include("top.php") ?>
	
<h3>Инвертировать строку</h3>
<form method="POST" action="">
    <p>Строка: <input name="x" type="text" autocomplete="off" value="<?=(isset($_POST['x']) ? $_POST['x'] : 'строка')?>" /></p>
    <input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $x = $_POST['x'];
    $y = iconv('windows-1251', 'utf-8', strrev(iconv('utf-8', 'windows-1251', $x)));
    echo 'Ответ: ';
    echo htmlspecialchars($y);
}
?>
	
<?php include("bottom.php") ?>
