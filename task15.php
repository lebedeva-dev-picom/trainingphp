﻿<?php include("top.php") ?>

<h3>JS + jQuery</h3>
<form>
	<p>Фамилия:</br><input id="p1" value="Иванов" /></p>
	<p>Имя:</br><input id="p2" value="Иван" /></p>
	<p>Отчество:</br><input id="p3" value="Иванович" /></p>
	<p>E-mail:</br><input id="p5" value="qwerty@mail.mail.ru" /></p>
	<p>Телефон:</br><input id="p4" value="8 (999) 123-45-67" /></p>
	<p>Дата рождения:</br><input type="date" id="p6" value="1990-07-07" /></p>
	<p><input type="button" id="btn" value="Отправить" /></p>
</form>
<div id="msg"></div>

<?php include("bottom.php") ?>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script>
$(function()
{
    $('#btn').click(f);
});
	
function f()
{
	var p1 = $('#p1').val();
	var p2 = $('#p2').val();
	var p3 = $('#p3').val();
	var p4 = $('#p4').val();
	var p5 = $('#p5').val();
	var p6 = $('#p6').val();
	
	var str = '';
	var err = '';
    
	if (p1 == '' || p2 == '' || p3 == '' || p4 == '') {
		err += '<p>Поля не заполнены!</p>';
    }
		
	if (!(chkRus(p1) && chkRus(p2) && chkRus(p3))) {
		err += '<p>ФИО должны быть на русском языке!</p>';
    }
		
	if (!chkTel(p4)) {
		err += '<p>Неверный номер телефона!</p>';
    }
		
	if (!chkEml(p5)) {
		err += '<p>Неверный e-mail!</p>';
    }
		
	if (err != '') {
		str = 'Здравстуйте, ' + 
            getUsername(p1, p2, p3, p6) + 
            '!</p>' +
            '<p>Ваш E-mail: ' + p5 + '</p>' +
            '<p>Ваш телефон: ' + p4 + '</p>';
    } else {
        str = err;
    }
	
	$('#msg').html(str);
}

function chkRus(p)
{
	var reg = /[\u0400-\u04FF]+/;
	return p.match(reg) == p;
}

function chkTel(p)
{
	var reg = /^[0-9][0-9\s-\(\)]*[0-9]$/;
	return p.match(reg) == p;
}

function chkEml(p)
{
	var reg = /^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/;
	return p.match(reg) == p;
}

function getUsername(p1, p2, p3, p6)
{
	if (getAge(p6) > 18) return p1 + ' ' + p2 + ' ' + p3;
	else return p2;
}

function getAge(p)
{
	var now_date = new Date;
	var brt_date = new Date(p);
	var age = now_date - brt_date;
	return Math.floor(age / 1000 / 24 / 60 / 60 / 365);
}
</script>
