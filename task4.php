﻿<?php include("top.php") ?>

<h3>Диагонали таблицы</h3>
<p id="answer1"></p>
<form method="POST", action="">
    <input type="submit" name="submit" />
</form>

<?php
if ($_SERVER[REQUEST_METHOD] == 'POST' && $_POST['submit'] != '') {
    $arr = genArr();
    $a1 = getTbl($arr);
    $a2 = getDiag1($arr);
    $a3 = getDiag2($arr);
    echo $a1 . 
        '<p><span class="color1">' . $a2 . '</span></p>' . 
        '<p><span class="color2">' . $a3 . '</span></p>';
}

function getRandomChar()
{
	return chr(mt_rand(97,122));
}

function genArr()
{
	for ($i = 0; $i < 10; $i++) {
		for ($j = 0; $j < 10; $j++) {
			$arr[$i][$j] = getRandomChar();
        }
	}
	return $arr;
}

function getTbl($arr)
{
	$retval = '<table border="0px">';
	for ($i = 0; $i < 10; $i++) {
		$retval .= '<tr>';
		for ($j = 0; $j < 10; $j++) {
			$retval .= '<td';
			if ($i == $j) {
				$retval .= ' class="color1"';
            } elseif (($i + $j) == 9) {
				$retval .= ' class="color2"';
            }
			$retval .= '>';
			$retval .= $arr[$i][$j];
			$retval .= '</td>';
		}
		$retval .= '</tr>';
	}
	$retval .= '</table>';
	return $retval;
}

function getDiag1($arr)
{
	$retval = '';
	for ($i = 0; $i < 10; $i++) {
		for ($j = 0; $j < 10; $j++) {
			if ($i == $j) {
				$retval .= $arr[$i][$j].' ';
            }
        }
    }
	return $retval;
}

function getDiag2($arr)
{
	$retval = '';
	for ($i = 0; $i < 10; $i++) {
		for ($j = 0; $j < 10; $j++) {
			if (($i + $j) == 9) {
				$retval .= $arr[$i][$j].' ';
            }
        }
    }
	return $retval;
}
?>
		
<?php include("bottom.php") ?>